//
//  ViewController.m
//  TransformIssues
//
//  Created by Denis Voronov on 07/11/14.
//  Copyright (c) 2014 xtinger. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()
@property (weak, nonatomic) IBOutlet UIView *f;
@property (weak, nonatomic) IBOutlet UIView *p;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    self.f.layer.anchorPoint = CGPointZero;
    self.f.frame = CGRectMake(0, 0, 2000, 1000);

    CGAffineTransform scale1 = CGAffineTransformMakeScale(1, 1);
    CGAffineTransform trans1 = CGAffineTransformMakeTranslation(1024, 768);
    CGAffineTransform scale2 = CGAffineTransformMakeScale(0.5, 0.5);
    CGAffineTransform trans2 = CGAffineTransformMakeTranslation(0, 0);
    
    CGAffineTransform tr1 = CGAffineTransformConcat(scale1, trans1);
    CGAffineTransform tr2 = CGAffineTransformConcat(scale2, trans2);

    self.f.layer.affineTransform = tr1;
    [UIView animateWithDuration:30 delay:0 options:UIViewAnimationOptionCurveLinear
                     animations:^{
                         self.f.layer.affineTransform = tr2;
                     } completion:NULL];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
